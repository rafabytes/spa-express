<?php
/* Template Name: Custom Search */
get_header(); 
global $wp_query;
$not_singular = $wp_query->found_posts > 1 ? 'Foram encontrados ' . $wp_query->found_posts . ' resultados ' : 'Foi encontrado ' . $wp_query->found_posts . ' resultado'; // if found posts is greater than one echo results(plural) else echo result (singular)
?>
<main id="page-unidades" class="page">

    <section class="container lg page-header roxo">
        <div class="container sm">
            <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" class="search">
                <input type="text" name="s" placeholder="Digite o nome da sua cidade ou estado para pesquisar" />
                <input type="hidden" name="post_type" value="franquias" />
                <input type="submit" alt="Search" value="" />
            </form>
        </div>
    </section>

    <section id="resultados">
        <div class="container sm">
            <h3><?= $not_singular ?> para:</h3>
            <h2><?php echo htmlentities($s, ENT_QUOTES, 'UTF-8'); ?></h2>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="unidade d-flex" data-aos="fade-up">
                        <div class="cover">
                            <img src="<?php the_field('imagem_da_unidade'); ?>" alt="<?php the_title(); ?>">
                        </div>
                        <div class="detalhes">
                            <div class="texto">
                                <h6>Unidade: <?php the_title(); ?></h6>
                                <h3><?php the_field('subtexto'); ?></h3>
                                <p><?php the_field('descricao'); ?></p>
                            </div>
                            <ul class="d-flex align-self-end justify-content-between">

                                <?php if( have_rows('redes_sociais') ): 
                                    while( have_rows('redes_sociais') ): the_row(); ?>

                                    <li>
                                        <a href="<?php the_sub_field('link_rs'); ?>" class="<?php the_sub_field('icone_rs'); ?>">
                                            <div class="icone"></div>
                                            <div class="desc">
                                                <span><?php the_sub_field('titulo_rs'); ?></span>
                                                <?php the_sub_field('texto_descritivo'); ?>
                                            </div>
                                        </a>
                                    </li>

                                <?php endwhile; ?>
                                <?php endif; ?> 
                                
                            </ul>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <div id="no-results">
                    <h5>QUE PENA!</h5>
                    <h2>Ainda não temos nenhuma unidade na sua cidade.</h2>
                    <p>Mas talvez essa pode ser a oportunidade de você ser dona do próprio negócio.</p>
                    <a href="<?php bloginfo('url'); ?>/seja-uma-franqueada" class="btn">Seja uma franqueada</a>
                </div>
            <?php endif; ?>
        </div>
    </section>

</main>
<?php get_footer(); ?>