<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordpressGulpBoilerplate
 */

get_header();
$cat = get_the_category();
?>

<main id="page-blog" class="page">

	<div class="container">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php bloginfo('url'); ?>">Início</a></li>
				<li class="breadcrumb-item active" aria-current="page">Blog</li>
			</ol>
		</nav>
	</div>

	<article class="article-single">
		<div class="article-header container">
			<ul class="nav meta align-items-center">
				<li class="data"><?php the_time( 'd.m.Y' ); ?></li>
				<li class="cat"><a href="<?php echo get_category_link($cat[0]->term_id) ?>"><?php echo $cat[0]->cat_name; ?></a></li>
			</ul>
			<h2 class="hearted"><?php the_title(); ?></h2>
			<ul class="nav share align-items-center">
				<li class="title">COMPARTILHE:</li>
				<!-- <li><a href="#" class="email"></a></li> -->
				<li><a href="https://wa.me/?text=<?php the_title() ?> <?php the_permalink(); ?>" class="whatsapp"></a></li>
				<li><a href="tg://msg?text=<?php the_title() ?> <?php the_permalink(); ?>" class="telegram"></a></li>
				<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="facebook"></a></li>
				<li><a href="https://twitter.com/intent/tweet?text=<?php the_title() ?> <?php the_permalink(); ?>" class="twitter"></a></li>
				<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" class="linkedin"></a></li>
			</ul>
			<?php if (get_the_post_thumbnail_url()) : ?>
				<figure class="cover">
					<img src="<?php echo get_the_post_thumbnail_url(); ?>">
				</figure>
			<?php endif; ?>
		</div>
		<div class="article-body container sm">
			<?php the_content(); ?>
			<div class="owl-galeria owl-carousel">

				<?php if( have_rows('galeria') ): 
                    while( have_rows('galeria') ): the_row(); ?>

                    	<div class="item">
							<figure class="cover">
								<img class="img-fluid" src="<?php the_sub_field('foto'); ?>" alt="<?php the_sub_field(legenda); ?>" >
							</figure>
							<h6 class="legenda"><?php the_sub_field('legenda'); ?></h6>
						</div>                        

                <?php endwhile; ?>
                <?php endif; ?> 
				
			</div>
		</div>

		<section id="blog" class="article-footer">
			<div class="container">
				<div class="row align-items-center section-header">
					<div class="col-md-6 col-lg-5 offset-lg-1">
						<h2 class="hearted"><span class="outline">Leia<br>também</span></h2>
					</div>
				</div>
			</div>
			<div class="container articles">
				<div class="row justify-content-center">

					<?php
					$args = array(
						'post_type' => 'post',
						'post_status' => 'publish',
						'cat' => $cat[0]->term_id,
						'posts_per_page' => 3,
						'orderby' => 'title',
						'order' => 'ASC',
					);
					$categories = get_categories();
					?>
					<?php $loop = new WP_Query($args);
					while ($loop->have_posts()) : $loop->the_post(); ?>

						<?php
						$category = get_the_category();
						$firstCategory = $category[0]->cat_name;
						$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
						?>

						<div class="col-md-4">
							<a href="<?php the_permalink(); ?>" class="d-block article" title="<?php the_title(); ?>">
								<div class="img">
									<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
								</div>
								<div class="cat"><?php echo $firstCategory; ?></div>
								<h1 class="resumo">
									<?php the_title(); ?>
								</h1>
							</a>
						</div>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>

				</div>
			</div>
		</section>
	</article>

	<section id="newsletter" class="container-fluid">
		<div class="container lg roxo">
			<h2 class="hearted branco center"><span class="outline">Receba nossa Newsletter</span></h2>
			<h3>Receba todas as nossas novidades no seu e-mail*</h3>
			<form action="#" class="newsletter">
				<input type="text" name="s" placeholder="Digite seu melhor e-mail" />
				<input type="submit" alt="Enviar" value="" />
			</form>
			<p>*ao assinar você permite contactá-lo dentro das normas da LGPD.</p>
		</div>
	</section>

</main>
<?php
get_footer();
