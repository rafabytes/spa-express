<?php
/* Template name: Contato Novo */
get_header();
?>
<main id="page-contato" class="page">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Início</a></li>
                <li class="breadcrumb-item active" aria-current="page">Fale Conosco</li>
            </ol>
        </nav>
    </div>

    <?php if (have_posts()) : ?>
        <section class="section-header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6" data-aos="fade-right">
                        <h2 class="hearted"><span class="outline">Contato</span></h2>
                    </div>
                    <div class="col-md-5 offset-md-1" data-aos="fade-left">
                        <div class="form-group">
                            <label for="#select_unidades">Selecione uma unidade</label>
                            <select class="custom-select" name="select_unidades" id="select_unidades">
                                <option value="">Selecionar unidade</option>
                                <?php
                                $args = array(
                                    'post_type' => 'franquias',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'orderby' => 'title',
                                    'order' => 'ASC',
                                );
                                ?>

                                <?php $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post(); ?>
                                    <option value="<?php the_field('email_unidade') ?>"><?php the_title() ?></option>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>

        <section class="form_contato" data-aos="fade-up">
            <div class="container form-unidade">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-header">
                            <h2>Dúvidas ou sugestões?</h2>
                            <p>Preencha o formulário e envie sua dúvida ou mensagem para que possamos lhe ajudar, estamos sempre dispostos a esclarecer suas dúvidas e dar todo o suporte necessário.</p>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <!-- <?php if (get_field('cod_do_formulario_1')) : ?>
                            <?php $formulario = get_field('cod_do_formulario_1');  ?>
                            <?php echo do_shortcode("" . $formulario . ""); ?>
                            <?php endif; ?> -->
                        <?php echo do_shortcode('[contact-form-7 id="463" title="Dúvidas ou Sugestões?"]'); ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <ul class="nav nav-pills nav-justified align-items-stretch tabs-contato" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link d-flex justify-content-between align-items-center" id="franquia-tab" data-toggle="tab" href="#franquia" role="tab" aria-controls="franquia" aria-selected="false">
                            <div class="texto">
                                <h3>Quero abrir uma franquia</h3>
                                <p>Gostaria de saber mais informações sobre nossas franquia?</p>
                            </div>
                            <img class="icone" src="<?php echo get_template_directory_uri() ?>/images/icons/icn-franquia.svg">
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link d-flex justify-content-between align-items-center" id="franqueada-tab" data-toggle="tab" href="#franqueada" role="tab" aria-controls="franqueada" aria-selected="false">
                            <div class="texto">
                                <h3>Sou franqueada</h3>
                                <p>Precisa de ajuda com sua unidade?</p>
                            </div>
                            <img class="icone" src="<?php echo get_template_directory_uri() ?>/images/icons/icn-franqueada.svg">
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="contatoTabContent">
                    <div class="tab-pane fade" id="franquia" role="tabpanel" aria-labelledby="franquia-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-header">
                                    <h2>Quer levar o SPA Express<br> para sua cidade??</h2>
                                    <p>Preencha o formulário para que possamos guiar* você na sua jornada em ser dona do próprio negócio e ter a liberdade financeira que sempre sonhou.</p>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <?php if (get_field('cod_do_formulario_2')) : ?>
                                    <?php $formulario = get_field('cod_do_formulario_2');  ?>
                                    <?php echo do_shortcode("" . $formulario . ""); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="franqueada" role="tabpanel" aria-labelledby="franqueada-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-header">
                                    <h2 class="laranja">Está com dúvidas ou precisa de ajuda com sua unidade?</h2>
                                    <p>Preencha o formulário e envie sua dúvida ou mensagem para que possamos lhe ajudar, estamos sempre dispostos a esclarecer suas dúvidas e dar todo o suporte necessário.</p>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <?php if (get_field('cod_do_formulario_3')) : ?>
                                    <?php $formulario = get_field('cod_do_formulario_3');  ?>
                                    <?php echo do_shortcode("" . $formulario . ""); ?>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

</main>
<?php get_footer(); ?>