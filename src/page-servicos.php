<?php
/* Template name: Serviços */
get_header();
?>
<main id="page-servicos" class="page">

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php bloginfo('url'); ?>">Início</a></li>
                <li class="breadcrumb-item active" aria-current="page">Nossos serviços</li>
            </ol>
        </nav>
    </div>

    <?php if (have_posts()) : ?>
        <section class="section-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6" data-aos="fade-up">
                        <h2 class="hearted"><span class="outline">Nossos Serviços</span></h2>
                    </div>
                    <div class="col-md-5 offset-md-1" data-aos="fade-up">
                        <p><?php the_field('descricao_topo'); ?></p>
                        <p class="laranja"><?php the_field('descricao_laranja'); ?></p>
                    </div>
                </div>
            </div>
        </section>

        <section id="servicos-lista">
            <div class="container">
                <?php $cont = 2;
                if (have_rows('servico')) : ?>
                    <?php while (have_rows('servico')) : the_row(); ?>
                        <div class="row align-items-center servico">

                            <?php if ($cont % 2 == 0) : ?>

                                <div class="col-md-6" data-aos="fade-up">
                                    <img class="img-fluid" src="<?php the_sub_field('imagem_serviço'); ?>" alt="">
                                </div>

                                <div class="col-md-5 offset-md-1" data-aos="fade-up">
                                    <div class="servico-header">
                                        <h3><?php the_sub_field('titulo'); ?></h3>
                                        <h2><?php the_sub_field('subtitulo'); ?></h2>
                                    </div>
                                    <ul class="flex-column">
                                        <?php if (have_rows('itens')) : ?>
                                            <?php while (have_rows('itens')) : the_row(); ?>
                                                <li><?php the_sub_field('item'); ?></li>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>

                            <?php else : ?>

                                <div class="col-md-5 offset-md-1" data-aos="fade-up">
                                    <div class="servico-header">
                                        <h3><?php the_sub_field('titulo'); ?></h3>
                                        <h2><?php the_sub_field('subtitulo'); ?></h2>
                                    </div>
                                    <ul class="flex-column">
                                        <?php if (have_rows('itens')) : ?>
                                            <?php while (have_rows('itens')) : the_row(); ?>
                                                <li><?php the_sub_field('item'); ?></li>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="col-md-6" data-aos="fade-up">
                                    <img class="img-fluid" src="<?php the_sub_field('imagem_serviço'); ?>" alt="">
                                </div>

                            <?php endif; ?>

                        </div>
                    <?php $cont++;
                    endwhile; ?>
                <?php endif; ?>

            </div>
        </section>
    <?php endif; ?>

</main>
<?php get_footer(); ?>