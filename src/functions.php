<?php

/**
 * WordpressGulpBoilerplate functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordpressGulpBoilerplate
 */

if (!function_exists('wpgb_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wpgb_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on WordpressGulpBoilerplate, use a find and replace
		 * to change 'wpgb' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('wpgb', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(array(
			'menu-1' => esc_html__('Primary', 'wpgb'),
		));

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('wpgb_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		)));

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support('custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		));
	}
endif;
add_action('after_setup_theme', 'wpgb_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wpgb_content_width()
{
	$GLOBALS['content_width'] = apply_filters('wpgb_content_width', 640);
}
add_action('after_setup_theme', 'wpgb_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wpgb_widgets_init()
{
	register_sidebar(array(
		'name'          => esc_html__('Sidebar', 'wpgb'),
		'id'            => 'sidebar-1',
		'description'   => esc_html__('Add widgets here.', 'wpgb'),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}
add_action('widgets_init', 'wpgb_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function wpgb_scripts()
{
	wp_enqueue_style('wpgb-style', get_stylesheet_uri());
	wp_enqueue_script('gmaps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDbBHqDtxuI3kcrRQYhy7lzpcqn0GOELuM&libraries=places');
	wp_enqueue_script('vendors', get_template_directory_uri() . '/js/vendors.min.js');
	wp_enqueue_script('scripts', get_template_directory_uri() . '/js/scripts.js');
}
add_action('wp_enqueue_scripts', 'wpgb_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Register Custom Navigation Walker
 */
function register_navwalker()
{
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');

function template_chooser($template)
{
	global $wp_query;
	$post_type = get_query_var('post_type');
	if ($wp_query->is_search && $post_type == 'franquias') {
		return locate_template('archive-franquias.php');  //  redirect to archive-search.php
	}
	return $template;
}
add_filter('template_include', 'template_chooser');

/**
 * Enqueue Ajax.
 */

function myplugin_ajaxurl()
{
	echo '<script type="text/javascript">
	var ajaxurl = "' . admin_url('admin-ajax.php') . '";
	</script>';
}
add_action('wp_head', 'myplugin_ajaxurl');

//add class to menu items
function add_menu_link_class($atts, $item, $args)
{
	if (property_exists($args, 'link_class')) {
		$atts['class'] = $args->link_class;
	}
	return $atts;
}
add_filter('nav_menu_link_attributes', 'add_menu_link_class', 1, 3);

function add_menu_list_item_class($classes, $item, $args)
{
	if (property_exists($args, 'list_item_class')) {
		$classes[] = $args->list_item_class;
	}
	return $classes;
}
add_filter('nav_menu_css_class', 'add_menu_list_item_class', 1, 3);

//spa_load_franquias_uf
add_action('wp_ajax_nopriv_spa_load_franquias_uf', 'spa_load_franquias_uf');
add_action('wp_ajax_spa_load_franquias_uf', 'spa_load_franquias_uf');
function spa_load_franquias_uf()
{
	// global $wp_query;
	global $post;
	$uf = $_POST['uf'];

	$args = array(
		'post_type' => 'franquias',
		'meta_key' => 'estados',
		'meta_value' => $uf,
		'posts_per_page' => -1,
		'order' => 'DESC'
	);

	$loop = new WP_Query($args);

	ob_start();

	if ($loop->have_posts()) {

		$estado = renameUf($uf);
		echo '<h3>' . $estado . '</h3>';
		echo '<ul class="nav flex-column">';
		while ($loop->have_posts()) : $loop->the_post();
			echo '<li>' . get_field('n_unidades') . ' - ' . get_the_title() . '</li>';
		endwhile;
		echo '</ul>';
		wp_reset_query();
	} else {

		echo '<h3 class="no-results">Nenhuma unidade encontrada!</h3';
	}

	$response = ob_get_contents();
	ob_end_clean();

	echo $response;
	die(1);
}

//spa_load_franquias_search
add_action('wp_ajax_nopriv_spa_load_franquias_search', 'spa_load_franquias_search');
add_action('wp_ajax_spa_load_franquias_search', 'spa_load_franquias_search');
function spa_load_franquias_search()
{
	// global $wp_query;
	global $post;
	$search_term = $_POST['search_term'];

	$args = array(
		'post_type' => 'franquias',
		's' => $search_term,
		'meta_key' => 'estados',
		'orderby' => 'meta_value',
		'posts_per_page' => -1,
		'order' => 'ASC'
	);

	$loop = new WP_Query($args);

	ob_start();

	if ($loop->have_posts()) {
		$uf_anterior = "";
		while ($loop->have_posts()) : $loop->the_post();
			$uf = get_field('estados');
			if ($loop->current_post === 0) {
				$uf_anterior = get_field('estados');
			}
			if ($loop->current_post !== 0 && $uf !== $uf_anterior) {
				echo '</ul>';
			}
			if ($loop->current_post === 0 || $uf !== $uf_anterior) {
				$estado = renameUf($uf);
				echo '<h3>' . $estado . '</h3>';
				echo '<ul class="nav flex-column" data-uf=' . $uf . '>';
			}
			echo '<li>' . get_field('n_unidades') . ' - ' . get_the_title() . '</li>';
			if ($uf !== $uf_anterior) {
				// echo '</ul>';
			}
			$uf_anterior = $uf;
		endwhile;
		wp_reset_query();
	} else {
		echo '<h3 class="no-results">Nenhuma unidade encontrada!</h3';
	}

	$response = ob_get_contents();
	ob_end_clean();

	echo $response;
	die(1);
}

function renameUf($uf)
{
	switch ($uf) {
		case "ac":
			$estado = "Acre";
			break;
		case "al":
			$estado = "Alagoas";
			break;
		case "am":
			$estado = "Amazonas";
			break;
		case "ap":
			$estado = "Amapá";
			break;
		case "ba":
			$estado = "Bahia";
			break;
		case "ce":
			$estado = "Ceará";
			break;
		case "df":
			$estado = "Distrito Federal";
			break;
		case "es":
			$estado = "Espírito Santo";
			break;
		case "go":
			$estado = "Goiás";
			break;
		case "ma":
			$estado = "Maranhão";
			break;
		case "mg":
			$estado = "Minas Gerais";
			break;
		case "ms":
			$estado = "Mato Grosso do Sul";
			break;
		case "mt":
			$estado = "Mato Grosso";
			break;
		case "pa":
			$estado = "Pará";
			break;
		case "pb":
			$estado = "Paraíba";
			break;
		case "pe":
			$estado = "Pernambuco";
			break;
		case "pi":
			$estado = "Piauí";
			break;
		case "pr":
			$estado = "Paraná";
			break;
		case "rj":
			$estado = "Rio de Janeiro";
			break;
		case "rn":
			$estado = "Rio Grande do Norte";
			break;
		case "ro":
			$estado = "Rondônia";
			break;
		case "rr":
			$estado = "Roraima";
			break;
		case "rs":
			$estado = "Rio Grande do Sul";
			break;
		case "sc":
			$estado = "Santa Catarina";
			break;
		case "se":
			$estado = "Sergipe";
			break;
		case "sp":
			$estado = "São Paulo";
			break;
		case "to":
			$estado = "Tocantíns";
			break;
		default:
			$estado = "";
			break;
	}

	return $estado;
}
