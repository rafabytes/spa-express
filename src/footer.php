<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

?>

<section id="seja-franqueado">
	<div class="container">
		<div class="row align-items-center section-header">
			<div class="col-md-6 col-lg-5 offset-lg-1" data-aos="fade-right" <?php echo (is_front_page()) ? 'data-aos-offset="-300"' : ''; ?>>
				<h2 class="hearted"><span class="outline">Seja uma<br>franqueada</span></h2>
			</div>
			<div class="col-md-6 col-lg-5 offset-lg-1" data-aos="fade-left" <?php echo (is_front_page()) ? 'data-aos-offset="-300"' : ''; ?>>
				<p>Serviços de estética,<br>tratamentos corporais<br>e faciais em domicílio.</p>
				<a href="<?php bloginfo('url'); ?>/contato" class="seta" title="<?php bloginfo('name'); ?> - Fale Conosco">Fale conosco</a>
			</div>
		</div>
	</div>
</section>

</div><!-- #content -->

<footer id="mainFooter" class="site-footer">
	<div class="container">
		<div id="owl-instagram" class="owl-carousel">
			<?php
                $args = array(  
                    'post_type' => 'fotos_rodape',
                    'post_status' => 'publish',
                    'posts_per_page' => -1, 
                    'orderby' => 'title', 
                    'order' => 'ASC', 
                ); 
            ?>

             <?php $loop = new WP_Query( $args ); while ( $loop->have_posts() ) : $loop->the_post(); ?>             	
                                  
                <?php if( have_rows('lista_das_fotos_no_rodape') ): 
                	 while( have_rows('lista_das_fotos_no_rodape') ): the_row(); ?>

                		<div class="img">
							<img src="<?php the_sub_field('foto'); ?>" alt="Instagram">
						</div>

                <?php endwhile; ?>
                <?php endif; ?>  
                                    
             <?php endwhile; ?>
             <?php wp_reset_postdata(); ?>   
			
		</div>

		<?php $query = new WP_Query(array('p' => 331, 'post_type' => 'rodape')); ?>
		<?php if ($query->have_posts()) : ?>
			<?php while ($query->have_posts()) : ?>
				<?php $query->the_post(); ?>

				<div class="row justify-content-between">
					<div class="col-auto logo">
						<div class="d-flex align-items-center">
							<img src="<?php the_field('logo_rodape'); ?>" alt="<?php bloginfo('name'); ?>">
							<?php the_field('endereco'); ?>
						</div>
					</div>
					<div class="col-auto">
						<div class="d-flex align-items-center redes">
							<h5>Siga nossas redes:</h5>
							<ul class="nav social">
								<?php if (have_rows('redes_sociais')) :
									while (have_rows('redes_sociais')) : the_row(); ?>

										<li><a href="<?php the_sub_field('url'); ?>" class="<?php the_sub_field('icone'); ?>"></a></li>

									<?php $cont++;
									endwhile; ?>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="row justify-content-between align-items-center copyrights">
					<div class="col-auto">
						<ul class="nav">
							<li><?php the_field('copyright'); ?></li>
							<li><a href="<?php bloginfo('url'); ?>/politica-de-privacidade" title="<?php bloginfo('name'); ?> - Política de privacidade">Política de privacidade</a></li>
						</ul>
					</div>
					<div class="col-auto">
						<a class="qualitare" href="https://qualitare.com"></a>
					</div>
				</div>

			<?php endwhile; ?>
		<?php endif; ?>

	</div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>