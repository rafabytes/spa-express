<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

<main id="page-404" class="page">
	<section class="roxo">
		<div class="container">
			<h1 class="text-center"><span class="outline">404</h1>
			<h4 class="text-center">Página não encontrada!</h4>
			<div class="text-center"><a href="" class="btn">Ir para home</a></div>
		</div>
	</section>
</main>

<?php
get_footer();
