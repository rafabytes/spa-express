<?php

/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>
<main id="page-blog" class="page search">

	<div class="container">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Início</a></li>
				<li class="breadcrumb-item"><a href="<?php echo site_url('/blog') ?>">Blog</a></li>
				<li class="breadcrumb-item active" aria-current="page">Resultados para:&nbsp; <strong><?php echo get_search_query(); ?></strong></li>
			</ol>
		</nav>
	</div>

	<section class="section-header" data-aos="fade-up">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-8">
					<h2 class="hearted"><span class="outline">Resultados</span></h2>
				</div>
				<div class="col-md-4">
					<form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" class="search">
						<input type="text" name="s" placeholder="O que você procura?" value="<?php echo get_search_query(); ?>" />
						<input type="hidden" name="post_type" value="unidade" />
						<input type="submit" alt="Search" value="" />
					</form>
				</div>
			</div>
		</div>
	</section>

	<section id="blog">
		<div class="container articles">
			<div class="row">
				<?php
				if (have_posts()) :
					while (have_posts()) : the_post(); ?>

						<?php
						$category = get_the_category();
						$firstCategory = $category[0]->cat_name;
						$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
						?>

						<div class="col-md-4" data-aos="fade-up">
							<a href="<?php the_permalink(); ?>" class="d-block article" title="<?php the_title(); ?>">
								<div class="img">
									<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
								</div>
								<div class="cat"><?php echo $firstCategory; ?></div>
								<h1 class="resumo">
									<?php the_title(); ?>
								</h1>
							</a>
						</div>

				<?php endwhile;
				endif;
				wp_reset_postdata(); ?>
			</div>
		</div>
	</section>

	<section id="newsletter" class="container-fluid">
		<div class="container lg roxo" data-aos="fade-up">
			<h2 class="hearted branco center"><span class="outline">Receba nossa Newsletter</span></h2>
			<h3>Receba todas as nossas novidades no seu e-mail*</h3>
			<form action="#" class="newsletter">
				<input type="text" name="s" placeholder="Digite seu melhor e-mail" />
				<input type="submit" alt="Enviar" value="" />
			</form>
			<p>*ao assinar você permite contactá-lo dentro das normas da LGPD.</p>
		</div>
	</section>

</main>

<?php
get_footer();
