jQuery(document).ready(function ($) {

  AOS.init({
    duration: 800
  });

  $(window).on('scroll', function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 500) {
      $("body").addClass("menu-scroll");
    } else {
      $("body").removeClass("menu-scroll");
    }
  });

  $('.menu-toggle').on('click', function (e) {
    $('body').toggleClass('menu-open');
  });

  if ($('.owl-galeria').length) {

    $('.owl-galeria').owlCarousel({
      margin: 24,
      dots: true,
      nav: false,
      loop: true,
      slideTransition: 'linear',
      items: 1
    });

  }

  if ($('#owl-instagram').length) {

    $('#owl-instagram').owlCarousel({
      margin: 16,
      dots: false,
      nav: false,
      autoplay: true,
      autoplayTimeout: 10000,
      smartSpeed: 10000,
      loop: true,
      center: true,
      slideTransition: 'linear',
      mouseDrag: false,
      freeDrag: false,
      responsive: {
        0: {
          items: 2
        },
        768: {
          items: 4
        },
        996: {
          items: 5
        }
      }
    });

    $('#owl-instagram').trigger('next.owl.carousel');

  }

  if ($('#owl-servicos').length) {

    $('#owl-servicos').owlCarousel({
      margin: 30,
      dots: false,
      nav: true,
      smartSpeed: 400,
      slideTransition: 'linear',
      mouseDrag: false,
      freeDrag: false,
      navText: ['<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M28.5 11L3 11M3 11L11 19M3 11L11 3" stroke="white" stroke-width="3" stroke-linecap="square"/></svg>', '<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 11H27.5M27.5 11L19.5 3M27.5 11L19.5 19" stroke="white" stroke-width="3" stroke-linecap="square"/></svg>'],
      responsive: {
        0: {
          items: 1
        },
        996: {
          items: 2
        }
      }
    });


    if ($('#owl-blog').length) {

      $('#owl-blog').owlCarousel({
        margin: 30,
        items: 1,
        dots: true,
        nav: false,
        smartSpeed: 400,
        slideTransition: 'linear'
      });

    }
  }

  if ($('#owl-depoimentos').length) {

    $('#owl-depoimentos').owlCarousel({
      margin: 15,
      dots: true,
      nav: false,
      smartSpeed: 400,
      slideTransition: 'linear',
      loop: true,
      mouseDrag: false,
      freeDrag: false,
      navText: ['<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M28.5 11L3 11M3 11L11 19M3 11L11 3" stroke="white" stroke-width="3" stroke-linecap="square"/></svg>', '<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 11H27.5M27.5 11L19.5 3M27.5 11L19.5 19" stroke="white" stroke-width="3" stroke-linecap="square"/></svg>'],
      responsive: {
        0: {
          items: 1
        },
        996: {
          items: 2,
          dots: false,
          nav: true
        }
      }
    });

  }

  if ($('#owl-depoimentos-1').length) {

    var sync1 = $('#owl-depoimentos-1'),
      sync2 = $('#owl-depoimentos-2')

    // Start Carousel
    sync1.owlCarousel({
      // rtl: true,
      // center: true,
      loop: true,
      items: 1,
      margin: 0,
      smartSpeed: 700,
      nav: true,
      dots: true,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      navContainer: '#owl-depoimentos-1-nav',
      dotsContainer: '#owl-depoimentos-1-dots',
      navText: ['<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M28.5 11L3 11M3 11L11 19M3 11L11 3" stroke="#76539C" stroke-width="3" stroke-linecap="square"/></svg>', '<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 11H27.5M27.5 11L19.5 3M27.5 11L19.5 19" stroke="#76539C" stroke-width="3" stroke-linecap="square"/></svg>'],
      animateOut: 'fadeOut'
    }).on('dragged.owl.carousel', function (e) {
      if (e.relatedTarget.state.direction == 'left') {
        sync2.trigger('next.owl.carousel')
      } else {
        sync2.trigger('prev.owl.carousel')
      }
    });

    sync2.owlCarousel({
      // rtl: true,
      loop: true,
      items: 1,
      margin: 14,
      smartSpeed: 700,
      nav: false,
      dots: false,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      // center: true,
      responsive: {
        996: {
          items: 2
        }
      }
    }).on('click', '.owl-item', function () {
      var i = $(this).index() - (thumbs + 1);
      sync2.trigger('to.owl.carousel', [i, duration, true]);
      sync1.trigger('to.owl.carousel', [i, duration, true]);
    });

    $('#owl-depoimentos-1-nav').on('click', '.owl-next', function () {
      sync2.trigger('next.owl.carousel')
    });
    $('#owl-depoimentos-1-nav').on('click', '.owl-prev', function () {
      sync2.trigger('prev.owl.carousel')
    });

  }

  if ($('#owl-depoimentos-3').length) {

    var sync3 = $('#owl-depoimentos-3'),
      sync4 = $('#owl-depoimentos-4')

    // Start Carousel
    sync3.owlCarousel({
      // rtl: true,
      // center: true,
      loop: true,
      items: 1,
      margin: 0,
      smartSpeed: 700,
      nav: true,
      dots: true,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      navContainer: '#owl-depoimentos-3-nav',
      dotsContainer: '#owl-depoimentos-3-dots',
      navText: ['<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M28.5 11L3 11M3 11L11 19M3 11L11 3" stroke="#76539C" stroke-width="3" stroke-linecap="square"/></svg>', '<svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 11H27.5M27.5 11L19.5 3M27.5 11L19.5 19" stroke="#76539C" stroke-width="3" stroke-linecap="square"/></svg>'],
      animateOut: 'fadeOut'
    }).on('dragged.owl.carousel', function (e) {
      if (e.relatedTarget.state.direction == 'left') {
        sync2.trigger('next.owl.carousel')
      } else {
        sync2.trigger('prev.owl.carousel')
      }
    });

    sync4.owlCarousel({
      // rtl: true,
      loop: true,
      items: 1,
      margin: 14,
      smartSpeed: 700,
      nav: false,
      dots: false,
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      animateOut: 'fadeOut'
    }).on('click', '.owl-item', function () {
      var i = $(this).index() - (thumbs + 1);
      sync4.trigger('to.owl.carousel', [i, duration, true]);
      sync3.trigger('to.owl.carousel', [i, duration, true]);
    });

    $('#owl-depoimentos-3-nav').on('click', '.owl-next', function () {
      sync4.trigger('next.owl.carousel')
    });
    $('#owl-depoimentos-3-nav').on('click', '.owl-prev', function () {
      sync4.trigger('prev.owl.carousel')
    });

  }

  //select unidade contato
  $("main").on("change", "#select_unidades", function () {
    $('.form-unidade').addClass('open')
    $('input[name="email_unidade"]').val($("#select_unidades option:selected").val())
  })

  //mapa franquias
  $('#mapa-franquias .heart, #mapa-franquias .text').on('click', function () {
    // alert($(this).attr('class').split(' ')[0]);
    var uf = $(this).attr('class').split(' ')[0];
    $('#search-unidades .search').val('')
    $('#mapa-franquias .active').removeClassSVG('active');
    $('#mapa-franquias .' + uf + '').addClassSVG('active');
    $.fn.spa_load_franquias_uf(uf);
  });

  //busca franquias
  $('#search-unidades').on('submit', function () {
    var search_term = $('#search-unidades .search').val();
    $.fn.spa_load_franquias_search(search_term);
  });

});

//filtrar mapa
$.fn.spa_load_franquias_uf = function (uf) {
  $.ajax({
    type: 'POST',
    url: ajaxurl,
    data: { "action": "spa_load_franquias_uf", uf: uf },
    success: function (response) {
      $("#resultados-mapa").html(response);
      return false;
    },
    error: function (error) { console.log(error) }
  });
}

//filtrar search
$.fn.spa_load_franquias_search = function (search_term) {
  $.ajax({
    type: 'POST',
    url: ajaxurl,
    data: { "action": "spa_load_franquias_search", search_term: search_term },
    success: function (response) {
      $("#resultados-mapa").html(response);
      $('#mapa-franquias .active').removeClassSVG('active');
      $('#resultados-mapa ul').each(function () {
        $('#mapa-franquias .' + $(this).data('uf') + '').addClassSVG('active');
      });
      return false;
    },
    error: function (error) { console.log(error) }
  });
}

/*
 * .addClassSVG(className)
 * Adds the specified class(es) to each of the set of matched SVG elements.
 */
$.fn.addClassSVG = function (className) {
  $(this).attr('class', function (index, existingClassNames) {
    return ((existingClassNames !== undefined) ? (existingClassNames + ' ') : '') + className;
  });
  return this;
};

/*
* .removeClassSVG(className)
* Removes the specified class to each of the set of matched SVG elements.
*/
$.fn.removeClassSVG = function (className) {
  $(this).attr('class', function (index, existingClassNames) {
    var re = new RegExp('\\b' + className + '\\b', 'g');
    return existingClassNames.replace(re, '');
  });
  return this;
};