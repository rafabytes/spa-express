<?php
get_header();
$term = get_queried_object();
?>
<main id="page-blog" class="page">

	<div class="container">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Início</a></li>
				<li class="breadcrumb-item"><a href="<?php echo site_url('/blog') ?>">Blog</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?php echo $term->name; ?></li>
			</ol>
		</nav>
	</div>

	<section class="section-header" data-aos="fade-up">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-8">
					<h2 class="hearted"><span class="outline"><?php echo $term->name; ?></span></h2>
				</div>
				<div class="col-md-4">
					<form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" class="search">
						<input type="text" name="s" placeholder="O que você procura?" value="<?php echo get_search_query(); ?>" />
						<input type="hidden" name="post_type" value="unidade" />
						<input type="submit" alt="Search" value="" />
					</form>
				</div>
			</div>
		</div>
	</section>

	<div class="container" data-aos="fade-up">
		<ul class="filtros nav nav-pills nav-fill align-items-center">
			<li class="nav-item nav-title">Filtre pelo assunto:</li>
			<li class="nav-item"><a href="<?php echo site_url('/blog') ?>" class="nav-link">Tudo</a></li>
			<?php
			$categorias = get_terms([
				'taxonomy' => 'category',
				'hide_empty' => true,
				'orderby' => 'slug',
				'parent' => 0
			]);
			foreach ($categorias as $categoria) :
				$term_link = get_term_link($categoria); ?>
				<li class="nav-item"><a class="nav-link <?php echo ($term->term_id === $categoria->term_id) ? 'active' : ''; ?>" href="<?php echo $term_link ?>"><?php echo $categoria->name ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>

	<section id="blog">
		<div class="container articles">
			<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="6" category="' . $term->slug . '" scroll="false" transition_container_classes="row" button_label="Mais notícias" button_loading_label="Carregando" button_done_label="Fim"]'); ?>
		</div>
	</section>

	<section id="newsletter" class="container-fluid">
		<div class="container lg roxo" data-aos="fade-up">
			<h2 class="hearted branco center"><span class="outline">Receba nossa Newsletter</span></h2>
			<h3>Receba todas as nossas novidades no seu e-mail*</h3>
			<form action="#" class="newsletter">
				<input type="text" name="s" placeholder="Digite seu melhor e-mail" />
				<input type="submit" alt="Enviar" value="" />
			</form>
			<p>*ao assinar você permite contactá-lo dentro das normas da LGPD.</p>
		</div>
	</section>

</main>

<?php
get_footer();
