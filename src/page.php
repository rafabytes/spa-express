<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

<main id="generic-page" class="page">

	<div class="container">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Início</a></li>
				<li class="breadcrumb-item active" aria-current="page"><?php the_title() ?></li>
			</ol>
		</nav>
	</div>

	<div class="container">
		<h1><?php the_title() ?></h1>
		<div class="content">
			<?php the_content(); ?>
		</div>
	</div>

</main><!-- #main -->

<?php
get_footer();
