<?php
/* Template name: Sobre */
get_header();
?>
<main id="page-sobre" class="page">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php bloginfo('url'); ?>">Início</a></li>
                <li class="breadcrumb-item active" aria-current="page">quem somos</li>
            </ol>
        </nav>
    </div>

    <?php if (have_posts()) : ?>

        <section id="fotos">

            <div class="container page-header" data-aos="fade-up">
                <h5><?php the_field('titulo_objetivo'); ?></h5>
                <h1><?php the_field('subtitulo_objetivo'); ?></h1>
                <a href="<?php the_field('link_objetivo'); ?>" title="<?php bloginfo('name'); ?> - Conheça nossos serviços" class="seta">Conheça nossos serviços</a>
            </div>

            <div class="container fotos-container" data-aos="fade-up">
                <img class="foto foto-01" src="<?php echo get_template_directory_uri(); ?>/images/sobre/img-header.png" data-aos="fade-up" data-aos-delay="100">
                <img class="foto foto-02" src="<?php echo get_template_directory_uri(); ?>/images/sobre/img-header.png" data-aos="fade-up" data-aos-delay="200">
                <img class="foto foto-03" src="<?php echo get_template_directory_uri(); ?>/images/sobre/img-header.png" data-aos="fade-up" data-aos-delay="200">
                <img class="foto foto-04" src="<?php echo get_template_directory_uri(); ?>/images/sobre/img-header.png" data-aos="fade-up" data-aos-delay="300">
                <img class="foto foto-05" src="<?php echo get_template_directory_uri(); ?>/images/sobre/img-header.png" data-aos="fade-up" data-aos-delay="300">
            </div>
        </section>

        <section id="numeros">
            <div class="container">
                <div class="row align-items-center section-header">
                    <div class="col-md-6 col-lg-5 offset-lg-1" data-aos="fade-right">
                        <h2 class="hearted"><span class="outline">O SPA em <br>números</span></h2>
                    </div>
                    <div class="col-md-6 col-lg-5 offset-lg-1" data-aos="fade-left">
                        <p><?php the_field('descricao_spa_numeros'); ?></p>
                        <a href="#" class="seta" title="<?php bloginfo('name'); ?> - Conhecer unidades">Conhecer unidades</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">

                    <?php $cont = 1;
                    if (have_rows('numeros_spa')) :
                        while (have_rows('numeros_spa')) : the_row(); ?>

                            <div class="col-md-4 col-numero" data-aos="fade-up">
                                <div class="numero numero-<?php echo $cont; ?>">
                                    <h2><?php the_sub_field('numero'); ?></h2>
                                    <h3><?php the_sub_field('titulo_numeros'); ?></h3>
                                    <p><?php the_sub_field('subtitulo_numeros'); ?></p>
                                </div>
                            </div>

                        <?php $cont++;
                        endwhile; ?>
                    <?php endif; ?>

                </div>
            </div>
        </section>

        <section id="valores">
            <div class="container">
                <div class="row">
                    <div class="col-md-6" data-aos="fade-right">
                        <img src="<?php the_field('img_destaque'); ?>" alt="<?php bloginfo('name'); ?> - Visão" class="img-fluid">
                    </div>
                    <div class="col-md-6" data-aos="fade-left">
                        <ul class="valores-nav nav flex-column">
                            <li class="nav-item d-flex align-items-start">
                                <img src="<?php echo get_template_directory_uri() ?>/images/icons/icn-heart.svg" alt="" class="icone">
                                <div class="texto">
                                    <?php if (get_field('caracteristica_1')) : ?>
                                        <h3><?php the_field('caracteristica_1'); ?></h3>
                                        <p><?php the_field('descricao_1'); ?></p>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <li class="nav-item d-flex align-items-start">
                                <img src="<?php echo get_template_directory_uri() ?>/images/icons/icn-heart.svg" alt="" class="icone">
                                <div class="texto">
                                    <?php if (get_field('caracteristica_2')) : ?>
                                        <h3><?php the_field('caracteristica_2'); ?></h3>
                                        <p><?php the_field('descricao_2'); ?></p>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <li class="nav-item d-flex align-items-start">
                                <img src="<?php echo get_template_directory_uri() ?>/images/icons/icn-heart.svg" alt="" class="icone">
                                <div class="texto">
                                    <?php if (get_field('caracteristica_3')) : ?>
                                        <h3><?php the_field('caracteristica_3'); ?></h3>
                                        <p><?php the_field('descricao_3'); ?></p>
                                    <?php endif; ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="timeline">
            <div class="container">
                <div class="row align-items-center section-header" data-aos="fade-up">
                    <div class="col-md-6 col-lg-5 offset-lg-1">
                        <h2 class="hearted"><span class="outline">Linha do <br>tempo</span></h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-md-1">
                        <?php $cont = 2; if( have_rows('item_linha') ): 
                        while( have_rows('item_linha') ): the_row(); ?>

                            <?php if ($cont % 2 == 0): ?>
                                <div class="timeline-row row flex-row-reverse">
                            <?php else: ?>
                                <div class="timeline-row row">
                            <?php endif; ?>
                                    <div class="ano col-md-6" <?php echo ($cont % 2 == 0) ? 'data-aos="fade-right"' : 'data-aos="fade-left"' ; ?>>
                                        <h3><?php the_sub_field('mes_e_ano_tempo'); ?></h3>
                                        <p><?php the_sub_field('descricao_tempo'); ?></p>
                                    </div>
                                </div>                        
                        <?php $cont++; endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>

        <section id="destaques">
            <div class="container">
                <div class="row align-items-center section-header">
                    <div class="col-md-6 col-lg-5 offset-lg-1" data-aos="fade-right">
                        <h2 class="hearted"><span class="outline">Nossos <br>destaques</span></h2>
                    </div>
                    <div class="col-md-6 col-lg-5 offset-lg-1" data-aos="fade-left">
                        <p><?php the_field('descricao_destaques'); ?></p>
                        <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?> - Nossos Destaques" class="seta">Ver todas as matérias</a>
                    </div>
                </div>
            </div>
            <div class="container logos-container">
                <div class="row align-items-center">

                    <?php $cont = 1;
                    if (have_rows('logos_destaques')) :
                        while (have_rows('logos_destaques')) : the_row(); ?>

                            <div class="col-6 col-md-3" data-aos="fade-up">
                                <a href="<?php the_sub_field('link_destaque'); ?>">
                                    <img src="<?php the_sub_field('logo'); ?>" alt="Nossos Destaques - <?php the_sub_field('nome_empresa'); ?>" class="logo img-fluid">
                                </a>
                            </div>

                        <?php $cont++;
                        endwhile; ?>
                    <?php endif; ?>

                </div>
            </div>
        </section>

    <?php endif; ?>

</main>
<?php get_footer(); ?>