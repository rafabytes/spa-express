<?php
/* Template name: Blog */
get_header();
?>
<main id="page-blog" class="page">

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Início</a></li>
                <li class="breadcrumb-item active" aria-current="page">Blog</li>
            </ol>
        </nav>
    </div>

    <section class="section-header" data-aos="fade-up">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h2 class="hearted"><span class="outline">Blog SPA Express</span></h2>
                </div>
                <div class="col-md-4">
                    <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" class="search">
                        <input type="text" name="s" placeholder="O que você procura?" />
                        <input type="hidden" name="post_type" value="unidade" />
                        <input type="submit" alt="Search" value="" />
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="container" data-aos="fade-up">
        <ul class="filtros nav nav-pills nav-fill align-items-center">
            <li class="nav-item nav-title">Filtre pelo assunto:</li>
            <li class="nav-item"><a href="#" class="nav-link active">Tudo</a></li>
            <?php
            $categorias = get_terms([
                'taxonomy' => 'category',
                'hide_empty' => true,
                'orderby' => 'slug',
                'parent' => 0
            ]);
            foreach ($categorias as $categoria) :
                $term_link = get_term_link($categoria); ?>
                <li class="nav-item"><a class="nav-link" href="<?php echo $term_link ?>"><?php echo $categoria->name ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>

    <section id="blog">
        <div class="container articles">
            <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="6" scroll="false" transition_container_classes="row" button_label="Mais notícias" button_loading_label="Carregando" button_done_label="Fim"]'); ?>
            <!-- <div class="row">

                <?php
                $args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => 6,
                    'orderby' => 'title',
                    'order' => 'ASC',
                );
                $categories = get_categories();
                ?>
                <?php $loop = new WP_Query($args);
                while ($loop->have_posts()) : $loop->the_post(); ?>

                    <?php
                    $category = get_the_category();
                    $firstCategory = $category[0]->cat_name;
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                    ?>

                    <div class="col-md-4" data-aos="fade-up">
                        <a href="<?php the_permalink(); ?>" class="d-block article" title="<?php the_title(); ?>">
                            <div class="img">
                                <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
                            </div>
                            <div class="cat"><?php echo $firstCategory; ?></div>
                            <h1 class="resumo">
                                <?php the_title(); ?>
                            </h1>
                        </a>
                    </div>

                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

            </div> -->
        </div>
        <!-- <div class="text-center">
            <a href="#" class="btn" data-aos="fade-up">Mais notícias</a>
        </div> -->
    </section>

    <section id="newsletter" class="container-fluid" data-aos="fade-up">
        <div class="container lg roxo">
            <h2 class="hearted branco center"><span class="outline">Receba nossa Newsletter</span></h2>
            <h3>Receba todas as nossas novidades no seu e-mail*</h3>
            <form action="#" class="newsletter">
                <input type="text" name="s" placeholder="Digite seu melhor e-mail" />
                <input type="submit" alt="Enviar" value="" />
            </form>
            <p>*ao assinar você permite contactá-lo dentro das normas da LGPD.</p>
        </div>
    </section>

</main>
<?php get_footer(); ?>