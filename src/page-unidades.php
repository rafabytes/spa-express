<?php
/* Template name: Unidades */
get_header();
?>
<main id="page-unidades" class="page">

    <section class="container lg page-header roxo">
        <div class="container">
            <h2 class="hearted branco center"><span class="outline">Conheça nossas unidades</span></h2>
            <h4><?php the_field('descricao'); ?></h4>
            <div class="container sm">
                <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform" class="search">
                    <input type="text" name="s" placeholder="Digite o nome da sua cidade ou estado para pesquisar" />
                    <input type="hidden" name="post_type" value="franquias" />
                    <input type="submit" alt="Search" value="" />
                </form>
            </div>
            <p class="small"><?php the_field('descricao_busca'); ?></p>
        </div>
    </section>


</main>
<?php get_footer(); ?>